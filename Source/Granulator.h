/*
  ==============================================================================

    Granulator.h
    Created: 26 Oct 2017 2:04:45pm
    Author:  Edward Lau

  ==============================================================================
*/

#ifndef GRANULATOR_H_
#define GRANULATOR_H_

#include "../JuceLibraryCode/JuceHeader.h"
#include "Grain.h"

class Granulator
{
public:
    Granulator();
    ~Granulator();
    
    struct Parameters
    {
        Parameters() noexcept
        : inputLevel     (0.5),
          blend          (0.5),
          randomLevel    (0.0),
          randomPan      (0.0),
          readPosition   (0.5),
          randomPosition (0.5),
          pitch          (0.0),
          randomPitch    (0.0),
          duration       (0.1),
          randomDuration (0.0),
          density        (100.0),
          isFrozen       (false)
        {}
        
        float inputLevel;       // -60.0 - 0.0 (dB)
        float blend;            // 0.0 - 1.0 Dry/Wet
        float randomLevel;      // -40.0 - 0.0 (dB +/-)
        float randomPan;        // -1.0 - 1.0 (+/-)
        float readPosition;     // 0.0 - 1.0 (relative read position)
        float randomPosition;   // 0.01 - 1.0 (+/-)
        float pitch;            // -12.0 - 12.0 (temp, semitones)
        float randomPitch;      // 0.0 - 12.0 (semitones +/-)
        float duration;         // 0.016 - 0.9 (seconds)
        float randomDuration;   // 0.0 - 0.87 (seconds +/-)
        float density;          // 1 - 500 (grain density per unit time)
        bool isFrozen;
    };
    
    const Parameters& getParameters() { return parameters; };
    void setParameters (const Parameters& newParameters);
    void setSampleRate (double sampleRate);
    
    void processBlock  (AudioSampleBuffer& buffer, const int totalInputChannels, const int totalOutputChannels);

private:
    static const int bufferSize = 192000;
    static const int maxDelayTime = 2.0;
    static const int grainEnvelopeSize = 1024;
    static const int numGrains = 60;
    int interonsetTimeSamples = 1000;
    float currentSampleRate;
    float twelfthRootOfTwo = powf (2, 1.0f / 12);

    AudioSampleBuffer delayBuffer;
    float* pDelayBuffer;
    int delayTimeSamples;
    int writePosition = 0;
    
    float grainEnvelopeTable [grainEnvelopeSize + 1];

    Grain grains[numGrains];
    Parameters parameters;
    LinearSmoothedValue<float> inputLevel, blend;
    Random random;
    
    float semitonesToLinear (const float semitones);
    void generateEnvelope   (float* table, const int length);
};

#endif
