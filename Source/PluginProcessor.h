/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Granulator.h"

//==============================================================================
/**
*/
class GranularPluginAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    GranularPluginAudioProcessor();
    ~GranularPluginAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    //==============================================================================
    void setInputLevel     (float level);
    void setBlend          (float blend);
    void setRandomLevel    (float randomLevel);
    void setRandomPan      (float randomPan);
    
    void setReadPosition   (float readPosition);
    void setRandomPosition (float randomPosition);
    
    void setPitch          (float pitch);
    void setRandomPitch    (float randomPitch);
    
    void setDuration       (float duration);
    void setRandomDuration (float randomDuration);
    
    void setDensity        (float density);
    
    void setFreeze         (bool freeze);
    
    //==============================================================================
    AudioParameterFloat* inputLevelParam;
    AudioParameterFloat* blendParam;
    AudioParameterFloat* randomLevelParam;
    AudioParameterFloat* randomPanParam;
    
    AudioParameterFloat* readPositionParam;
    AudioParameterFloat* randomPositionParam;
    
    AudioParameterFloat* pitchParam;
    AudioParameterFloat* randomPitchParam;
    
    AudioParameterFloat* durationParam;
    AudioParameterFloat* randomDurationParam;
    
    AudioParameterFloat* densityParam;
    
    AudioParameterBool* freezeParam;
    
private:
    //==============================================================================
    Granulator granulator;
    Granulator::Parameters granulatorParams;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GranularPluginAudioProcessor)
};
