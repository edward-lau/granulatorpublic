/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
GranularPluginAudioProcessor::GranularPluginAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
    granulatorParams = granulator.getParameters();
    
    addParameter (inputLevelParam       = new AudioParameterFloat ("inputLevel", "Input Level", -60.0, 0.0, -12.0));
    addParameter (blendParam            = new AudioParameterFloat ("blend", "Blend", 0.0, 1.0, 0.5));
    addParameter (randomLevelParam      = new AudioParameterFloat ("randomLevel", "Random Level", -40.0, 0.0, -6.0));
    addParameter (randomPanParam        = new AudioParameterFloat ("randomPan", "Random Pan", 0.0, 1.0, 0.0));
    addParameter (readPositionParam     = new AudioParameterFloat ("readPosition", "Read Position", 0.0, 1.0, 0.5));
    addParameter (randomPositionParam   = new AudioParameterFloat ("randomPosition", "Random Position", 0.01, 1.0, 0.5));
    addParameter (pitchParam            = new AudioParameterFloat ("pitch", "Pitch", -12.0, 12.0, 0.0));
    addParameter (randomPitchParam      = new AudioParameterFloat ("randomPitch", "Random Pitch", 0.0, 12.0, 0.0));
    addParameter (durationParam         = new AudioParameterFloat ("duration", "Duration", 16.0, 900.0, 100.0));
    addParameter (randomDurationParam   = new AudioParameterFloat ("randomDuration", "Random Duration", 0.0, 870.0, 0.0));
    addParameter (densityParam          = new AudioParameterFloat ("density", "Density", 1, 500, 100));
    addParameter (freezeParam           = new AudioParameterBool  ("freeze", "Freeze", false));
}

GranularPluginAudioProcessor::~GranularPluginAudioProcessor()
{
}

//==============================================================================
const String GranularPluginAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool GranularPluginAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool GranularPluginAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool GranularPluginAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double GranularPluginAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int GranularPluginAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int GranularPluginAudioProcessor::getCurrentProgram()
{
    return 0;
}

void GranularPluginAudioProcessor::setCurrentProgram (int index)
{
}

const String GranularPluginAudioProcessor::getProgramName (int index)
{
    return {};
}

void GranularPluginAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void GranularPluginAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    granulator.setSampleRate (sampleRate);
    
    setInputLevel       (*inputLevelParam);
    setBlend            (*blendParam);
    setRandomLevel      (*randomLevelParam);
    setRandomPan        (*randomPanParam);
    setReadPosition     (*readPositionParam);
    setRandomPosition   (*randomPositionParam);
    setPitch            (*pitchParam);
    setRandomPitch      (*randomPitchParam);
    setDuration         (*durationParam);
    setRandomDuration   (*randomDurationParam);
    setDensity          (*densityParam);
    setFreeze           (false);
}

void GranularPluginAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool GranularPluginAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void GranularPluginAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
    
    granulator.processBlock (buffer, totalNumInputChannels, totalNumOutputChannels);
}


//==============================================================================
bool GranularPluginAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* GranularPluginAudioProcessor::createEditor()
{
    return new GranularPluginAudioProcessorEditor (*this);
}

//==============================================================================
void GranularPluginAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    
    ScopedPointer<XmlElement> xml (new XmlElement ("GranulatorSettings"));
    
    xml->setAttribute ("inputLevel",     (double) *inputLevelParam);
    xml->setAttribute ("blendParam",     (double) *blendParam);
    xml->setAttribute ("randomLevel",    (double) *randomLevelParam);
    xml->setAttribute ("randomPan",      (double) *randomPanParam);
    xml->setAttribute ("readPosition",   (double) *readPositionParam);
    xml->setAttribute ("randomPosition", (double) *randomPositionParam);
    xml->setAttribute ("pitch",          (double) *pitchParam);
    xml->setAttribute ("randomPitch",    (double) *randomPitchParam);
    xml->setAttribute ("duration",       (double) *durationParam);
    xml->setAttribute ("randomDuration", (double) *randomDurationParam);
    xml->setAttribute ("density",        (double) *densityParam);
    
    copyXmlToBinary (*xml, destData);
}

void GranularPluginAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    
    ScopedPointer<XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));
    
    if (xmlState != nullptr)
    {
        if (xmlState->hasTagName ("GranulatorSettings"))
        {
            *inputLevelParam     = xmlState->getDoubleAttribute ("inputLevel", 1.0);
            *blendParam          = xmlState->getDoubleAttribute ("blendParam", 1.0);
            *randomLevelParam    = xmlState->getDoubleAttribute ("randomLevel", 1.0);
            *randomPanParam      = xmlState->getDoubleAttribute ("randomPan", 1.0);
            *readPositionParam   = xmlState->getDoubleAttribute ("readPosition", 1.0);
            *randomPositionParam = xmlState->getDoubleAttribute ("randomPosition", 1.0);
            *pitchParam          = xmlState->getDoubleAttribute ("pitch", 1.0);
            *randomPitchParam    = xmlState->getDoubleAttribute ("randomPitch", 1.0);
            *durationParam       = xmlState->getDoubleAttribute ("duration", 1.0);
            *randomDurationParam = xmlState->getDoubleAttribute ("randomDuration", 1.0);
            *densityParam        = xmlState->getDoubleAttribute ("density", 1.0);
        }
    }
}

//==============================================================================
void GranularPluginAudioProcessor::setInputLevel (float inputLevel)
{
    granulatorParams.inputLevel = Decibels::decibelsToGain (inputLevel);
    granulator.setParameters (granulatorParams);
    *inputLevelParam = inputLevel;
}

void GranularPluginAudioProcessor::setBlend (float blend)
{
    granulatorParams.blend = blend;
    granulator.setParameters (granulatorParams);
    *blendParam = blend;
}

void GranularPluginAudioProcessor::setRandomLevel (float randomLevel)
{
    granulatorParams.randomLevel = randomLevel;
    granulator.setParameters (granulatorParams);
    *randomLevelParam = randomLevel;
}

void GranularPluginAudioProcessor::setRandomPan (float randomPan)
{
    granulatorParams.randomPan = randomPan;
    granulator.setParameters (granulatorParams);
    *randomPanParam = randomPan;
}

void GranularPluginAudioProcessor::setReadPosition (float readPosition)
{
    granulatorParams.readPosition = readPosition;
    granulator.setParameters (granulatorParams);
    *readPositionParam = readPosition;
}

void GranularPluginAudioProcessor::setRandomPosition (float randomPosition)
{
    granulatorParams.randomPosition = randomPosition;
    granulator.setParameters (granulatorParams);
    *randomPositionParam = randomPosition;
}

void GranularPluginAudioProcessor::setPitch (float pitch)
{
    granulatorParams.pitch = pitch;
    granulator.setParameters (granulatorParams);
    *pitchParam = pitch;
}

void GranularPluginAudioProcessor::setRandomPitch (float randomPitch)
{
    granulatorParams.randomPitch = randomPitch;
    granulator.setParameters (granulatorParams);
    *randomPitchParam = randomPitch;
}

void GranularPluginAudioProcessor::setDuration (float duration)
{
    granulatorParams.duration = duration / 1000;
    granulator.setParameters (granulatorParams);
    *durationParam = duration;
}

void GranularPluginAudioProcessor::setRandomDuration (float randomDuration)
{
    granulatorParams.randomDuration = randomDuration / 1000;
    granulator.setParameters (granulatorParams);
    *randomDurationParam = randomDuration;
}

void GranularPluginAudioProcessor::setDensity (float density)
{
    granulatorParams.density = density;
    granulator.setParameters (granulatorParams);
    *densityParam = density;
}

void GranularPluginAudioProcessor::setFreeze (bool freeze)
{
    granulatorParams.isFrozen = freeze;
    granulator.setParameters (granulatorParams);
    *freezeParam = freeze;
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new GranularPluginAudioProcessor();
}
