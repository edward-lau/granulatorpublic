/*
  ==============================================================================

    Granulator.cpp
    Created: 26 Oct 2017 2:04:45pm
    Author:  Edward Lau

  ==============================================================================
*/ 

#include "Granulator.h"

Granulator::Granulator ()
{
    delayBuffer = AudioSampleBuffer (1, bufferSize);
    delayBuffer.clear();
    pDelayBuffer = delayBuffer.getWritePointer (0);
    
    generateEnvelope (grainEnvelopeTable, grainEnvelopeSize);
    
    for (int grain = 0; grain < numGrains; ++grain)
    {
        grains[grain].initialize (grainEnvelopeTable, grainEnvelopeSize, pDelayBuffer, maxDelayTime);
    }
}

Granulator::~Granulator()
{
    
}

void Granulator::setParameters (const Parameters& newParameters)
{
    inputLevel.setValue (newParameters.inputLevel);
    blend.setValue (newParameters.blend);
    parameters = newParameters;
}

void Granulator::setSampleRate (double sampleRate)
{
    currentSampleRate = sampleRate;
    delayTimeSamples = (int)(maxDelayTime * sampleRate);
    
    for (int grain = 0; grain < numGrains; ++grain)
    {
        grains[grain].setSampleRate (sampleRate);
    }
    
    // reset LinearSmoothedValue objects
    const double smoothTime = 0.1;
    blend.reset (sampleRate, smoothTime);
    inputLevel.reset (sampleRate, smoothTime);
    setParameters (parameters); // reset LinearSmoothedValue parameters to previous settings
    
    random.setSeed (Time::currentTimeMillis());
}

void Granulator::processBlock (AudioSampleBuffer& buffer, const int totalNumInputChannels, const int totalNumOutputChannels)
{
    const int numSamples = buffer.getNumSamples();
    const float summingGain = 1.0 / totalNumInputChannels;
    float** pChannelData = buffer.getArrayOfWritePointers();
    
    for (int sample = 0; sample < numSamples; ++sample)
    {
        // TODO: separate vectors for inactive and active grains faster?
        // count down interonset time and activate grain if necessary
        --interonsetTimeSamples;
        if (interonsetTimeSamples <= 0) // TODO: do this outside the loop?
        {
            // find an inactive grain and activate
            for (int grain = 0; grain < numGrains; ++grain)
            {
                if (grains[grain].isActive == false)
                {
                    float grainDuration = parameters.duration + ((random.nextFloat() * 2.0 - 1.0) * parameters.randomDuration);
                    if (grainDuration < 0.016) // TODO: get rid of branching statement?
                    {
                        grainDuration += parameters.randomDuration;
                    }
                    else if (grainDuration > 0.9)
                    {
                        grainDuration -= parameters.randomDuration;
                    }
                    const int grainDurationSamples = (int)(grainDuration * currentSampleRate);
                    
                    // get grain relative read position
                    float readPosition = parameters.readPosition + ((random.nextFloat() * 2.0 - 1.0) * parameters.randomPosition);
                    if (readPosition < 0) // TODO: get rid of branching statement?
                    {
                        readPosition += parameters.randomPosition;
                    }
                    else if (readPosition > 1.0)
                    {
                        readPosition -= parameters.randomPosition;
                    }
                    
                    const float grainPitch = parameters.pitch + ((random.nextFloat() * 2.0 - 1.0) * parameters.randomPitch);
                    const float grainIncrement = semitonesToLinear (grainPitch);
                    
                    // get grain delay time (determines reading position of grain in buffer relative to writing position)
                    float grainDelayTime = readPosition * maxDelayTime;
                    const float endPositionTime = (grainIncrement * grainDuration) - grainDelayTime;

                    // check if the grain read position will overtake the write position (even with frozen buffer)
                    if (endPositionTime > 0)
                    {
                        grainDelayTime += endPositionTime;
                    }
                       
                    // check if the write position will overtake the grain read position
                    if ((grainDuration - endPositionTime) > maxDelayTime)
                    {
                        grainDelayTime -= endPositionTime;
                    }
                    
                    const float grainLevel = Decibels::decibelsToGain (random.nextFloat() * (parameters.randomLevel));
                    const float grainPan = (random.nextFloat() * 2.0 - 1.0) * parameters.randomPan;
                    
                    grains[grain].activate (grainDelayTime, grainLevel, grainIncrement, grainDurationSamples, grainPan, writePosition);
                    
                    break;
                }
            }
            
            // calculate next interonset time as a function of density
            const float interonsetTime = -logf(random.nextFloat()) / parameters.density;
            interonsetTimeSamples = (int)(interonsetTime * currentSampleRate);
        }
        
        // process active grains into the stereo output samples
        float grainOutputs[2] = { 0.0, 0.0 };
        for (int grain = 0; grain < numGrains; ++grain)
        {
            if (grains[grain].isActive)
            {
                grains[grain].processStereoSamples(grainOutputs);
            }
        }
        
        // feed the delay line with the sum of input channel data
        float sumInput = 0.0;
        const float inputLevelValue = inputLevel.getNextValue();
        for (int input = 0; input < totalNumInputChannels; ++input)
        {
            sumInput += pChannelData[input][sample];
        }
        pDelayBuffer[writePosition] = sumInput * summingGain * inputLevelValue;
        
        // scale inputs and grain outputs with blend parameter and write to stereo output
        const float blendValue = blend.getNextValue();
        int stereoChannel = 0;
        for (int channel = 0; channel < totalNumOutputChannels; ++channel)
        {
            pChannelData[stereoChannel][sample] *= (1 - blendValue);
            pChannelData[stereoChannel][sample] += (grainOutputs[stereoChannel] * blendValue);
            stereoChannel = (channel < 1 ? channel + 1 : 0);
        }
        
        // if buffer isn't frozen, increment writing position, modulo if necessary
        if (parameters.isFrozen == false)
        {
            writePosition = (writePosition < delayTimeSamples ? writePosition + 1 : 0);
        }
    }
}

void Granulator::generateEnvelope (float* table, const int length)
{
    // Hann envelope
    int i;
    
    for (i = 0; i < length; i++)
    {
        table[i] = 0.5 - 0.5 * cos (i * 2.0 * double_Pi / length);
    }
    
    table[i] = table[0];
}

float Granulator::semitonesToLinear (const float semitones)
{
    const float linear = powf (twelfthRootOfTwo, semitones);
    
    return linear;
}
