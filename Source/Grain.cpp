/*
  ==============================================================================

    Grain.cpp
    Created: 26 Oct 2017 2:04:55pm
    Author:  Edward Lau

  ==============================================================================
*/

#include "Grain.h"

Grain::Grain()
{
    
}

Grain::~Grain()
{
}

void Grain::initialize(float* envelope, const float envSize, float* delayBuffer, const float maxDelayTimeInit)
{
    pDelayBuffer = delayBuffer;
    pEnvelope = envelope;
    maxDelayTime = maxDelayTimeInit;
    envelopeSize = envSize;
}

void Grain::setSampleRate (double newSampleRate)
{
    sampleRate = newSampleRate;
}

void Grain::activate (float newDelayTime, const float newLevel, const float newPitch, const int durationSamples, float panPosition, int writePosition)
{
    level = newLevel;
    delayTime = newDelayTime; // set starting reading position in delay buffer relative to write position
    readPosition = writePosition;
    readIncrement = newPitch;
    envReadPos = 0;
    envIncrement = envelopeSize/durationSamples;
    durationCountDown = durationSamples;
    
    panPosition *= 0.5;
    panLeft  = sqrtf (0.5 - panPosition);
    panRight = sqrtf (panPosition + 0.5);
    
    isActive = true;
}

void Grain::processStereoSamples (float* samples)
{
    float delayTimeSamples = delayTime * sampleRate;
    const int maxDelayTimeSamples = (int)(maxDelayTime * sampleRate);
    
    if (delayTimeSamples > maxDelayTimeSamples)
    {
        delayTimeSamples = (float)maxDelayTimeSamples;
    }
        
    // get reading position
    float readPos = readPosition - delayTimeSamples;
    
    // check if reading position is within buffer range, modulo if necessary
    readPos = (readPos >= 0 ? (readPos < maxDelayTimeSamples ? readPos : readPos - maxDelayTimeSamples) : readPos + maxDelayTimeSamples);
    
    // delay buffer linear interpolation
    const int readIndex = (int)readPos; // get index to the left of the reading position
    const float frac = readPos - readIndex; // get the "fraction", distance of reading position to the index
    const int nextIndex = (readIndex != maxDelayTimeSamples - 1 ? readIndex + 1 : 0); // get the next index, wraparound if necessary
    
    // envelope table linear interpolation
    const int envIndex = (int)envReadPos;
    const float envFrac = envReadPos - envIndex;
    const int envNextIndex = envIndex + 1;
    
    // interpolate envelope table value
    const float envelopeOut = (pEnvelope[envIndex] + envFrac * (pEnvelope[envNextIndex] - pEnvelope[envIndex]));
    
    // interpolate delay buffer value and multiply with envelope
    const float output = (pDelayBuffer[readIndex] + frac * (pDelayBuffer[nextIndex] - pDelayBuffer[readIndex])) * envelopeOut;
    
    // grain panning
    samples[0] += (output * panLeft)  * level;
    samples[1] += (output * panRight) * level;
        
    // increment envelope reading position
    envReadPos += envIncrement;
    
    // increment reading position, modulo if necessary
    readPosition += readIncrement;
    readPosition = (readPosition < maxDelayTimeSamples ? readPosition : readPosition - maxDelayTimeSamples);
    
    // count down grain duration and deactivate if necessary
    --durationCountDown;
    if (durationCountDown <= 0)
    {
        isActive = false;
    }
}
