/*
  ==============================================================================

    Grain.h
    Created: 26 Oct 2017 2:04:55pm
    Author:  Edward Lau

  ==============================================================================
*/

#ifndef GRAIN_H_
#define GRAIN_H_

#include "../JuceLibraryCode/JuceHeader.h"

class Grain
{
public:
    Grain ();
    ~Grain();
    
    bool isActive = false; 

    void initialize            (float* envelope, const float envelopeSize, float* delayBuffer, const float maxDelayTime);
    void setSampleRate         (double sampleRate);
    void activate              (float delayTime, const float level, const float pitch, const int durationSamples, const float panPosition, const int writePosition);
    void processStereoSamples  (float* samples);
    
private:
    float* pDelayBuffer; // pointer to granulator delay buffer
    float* pEnvelope; // pointer to envelope buffer
    int* pDelayWritePosition;
    
    float level;
    float readPosition, readIncrement;
    float delayTime, maxDelayTime;
    float panLeft, panRight;
    float envReadPos, envIncrement, envelopeSize;
    float sampleRate;
    int durationCountDown, delayTimeSamples;
};

#endif
