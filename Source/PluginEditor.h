/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"


//==============================================================================
/**
*/
class GranularPluginAudioProcessorEditor  : public AudioProcessorEditor,
                                            Slider::Listener,
                                            Button::Listener,
                                            private Timer
{
public:
    GranularPluginAudioProcessorEditor (GranularPluginAudioProcessor&);
    ~GranularPluginAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    GranularPluginAudioProcessor& processor;
    
    Label inputLevelLabel;
    Slider inputLevelSlider;
    
    Label blendLabel;
    Slider blendSlider;
    
    Label randomLevelLabel;
    Slider randomLevelSlider;
    
    Label readPositionLabel;
    Slider readPositionSlider;
    
    Label randomPositionLabel;
    Slider randomPositionSlider;
    
    Label pitchLabel;
    Slider pitchSlider;
    
    Label randomPitchLabel;
    Slider randomPitchSlider;
    
    Label durationLabel;
    Slider durationSlider;
    
    Label randomDurationLabel;
    Slider randomDurationSlider;
    
    Label densityLabel;
    Slider densitySlider;
    
    Label randomPanLabel;
    Slider randomPanSlider;
    
    TextButton freezeButton;
    
    void sliderValueChanged (Slider* slider) override;
    void buttonClicked (Button* button) override;
    void updateFreezeButtonText();
    void timerCallback () override;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GranularPluginAudioProcessorEditor)
};
