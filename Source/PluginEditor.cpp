/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
GranularPluginAudioProcessorEditor::GranularPluginAudioProcessorEditor (GranularPluginAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Level
    addAndMakeVisible (inputLevelLabel);
    inputLevelLabel.setText ("Input Level", dontSendNotification);
    
    addAndMakeVisible (inputLevelSlider);
    inputLevelSlider.setRange (-60.0, 0.0);
    inputLevelSlider.setValue (*processor.inputLevelParam, dontSendNotification);
    inputLevelSlider.setTextBoxStyle (Slider::NoTextBox, false, 100, 20);
    inputLevelSlider.addListener (this);
    
    // Blend
    addAndMakeVisible (blendLabel);
    blendLabel.setText ("Blend", dontSendNotification);
    
    addAndMakeVisible (blendSlider);
    blendSlider.setRange (0.0, 1.0);
    blendSlider.setValue (*processor.blendParam, dontSendNotification);
    blendSlider.setTextBoxStyle (Slider::NoTextBox, false, 100, 20);
    blendSlider.addListener (this);
    
    // Random grain level (dB)
    addAndMakeVisible (randomLevelLabel);
    randomLevelLabel.setText ("Random Level", dontSendNotification);
    
    addAndMakeVisible (randomLevelSlider);
    randomLevelSlider.setRange (-40.0, 0.0);
    randomLevelSlider.setValue (*processor.randomLevelParam, dontSendNotification);
    randomLevelSlider.addListener (this);
    
    // Read Position (relative)
    addAndMakeVisible (readPositionLabel);
    readPositionLabel.setText ("Read Pos", dontSendNotification);
    
    addAndMakeVisible (readPositionSlider);
    readPositionSlider.setRange (0.0, 1.0);
    readPositionSlider.setValue (*processor.readPositionParam, dontSendNotification);
    readPositionSlider.addListener (this);
    
    // Random Position
    addAndMakeVisible (randomPositionLabel);
    randomPositionLabel.setText ("Random Pos", dontSendNotification);
    
    addAndMakeVisible (randomPositionSlider);
    randomPositionSlider.setRange (0.01, 1.0);
    randomPositionSlider.setValue (*processor.randomPositionParam, dontSendNotification);
    randomPositionSlider.addListener (this);
    
    // Pitch (semitones)
    addAndMakeVisible (pitchLabel);
    pitchLabel.setText ("Pitch", dontSendNotification);
    
    addAndMakeVisible (pitchSlider);
    pitchSlider.setRange (-12.0, 12.0);
    pitchSlider.setValue (*processor.pitchParam, dontSendNotification);
    pitchSlider.addListener (this);
    
    // Random pitch (semitones)
    addAndMakeVisible (randomPitchLabel);
    randomPitchLabel.setText ("Random Pitch", dontSendNotification);
    
    addAndMakeVisible (randomPitchSlider);
    randomPitchSlider.setRange (0.0, 12.0);
    randomPitchSlider.setValue (*processor.randomPitchParam, dontSendNotification);
    randomPitchSlider.addListener (this);
    
    // Duration (ms)
    addAndMakeVisible (durationLabel);
    durationLabel.setText ("Duration", dontSendNotification);
    
    addAndMakeVisible (durationSlider);
    durationSlider.setRange (16.0, 900.0);
    durationSlider.setValue (*processor.durationParam, dontSendNotification);
    durationSlider.addListener (this);
    
    // Random duration
    addAndMakeVisible (randomDurationLabel);
    randomDurationLabel.setText ("Random Dur", dontSendNotification);
    
    addAndMakeVisible (randomDurationSlider);
    randomDurationSlider.setRange (0.0, 870.0);
    randomDurationSlider.setValue (*processor.randomDurationParam, dontSendNotification);
    randomDurationSlider.addListener (this);
    
    // Density (grains per second?)
    addAndMakeVisible (densityLabel);
    densityLabel.setText ("Density", dontSendNotification);
    
    addAndMakeVisible (densitySlider);
    densitySlider.setRange (1, 500);
    densitySlider.setValue (*processor.densityParam, dontSendNotification);
    densitySlider.addListener (this);
    
    // Random panning
    addAndMakeVisible (randomPanLabel);
    randomPanLabel.setText ("Random Pan", dontSendNotification);
    
    addAndMakeVisible (randomPanSlider);
    randomPanSlider.setRange (0.0, 1.0);
    randomPanSlider.setValue (*processor.randomPanParam, dontSendNotification);
    randomPanSlider.addListener (this);
    
    // Freeze
    addAndMakeVisible (freezeButton);
    updateFreezeButtonText();
    freezeButton.addListener (this);
    
    setSize (400, 370);
    startTimer (42);
}

GranularPluginAudioProcessorEditor::~GranularPluginAudioProcessorEditor()
{
}

//==============================================================================
void GranularPluginAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void GranularPluginAudioProcessorEditor::resized()
{
    inputLevelLabel.setBounds       (10, 10, 90, 20);
    inputLevelSlider.setBounds      (100, 10, getWidth() - 110, 20);
    
    blendLabel.setBounds            (10, 40, 90, 20);
    blendSlider.setBounds           (100, 40, getWidth() - 110, 20);
    
    randomLevelLabel.setBounds      (10, 70, 90, 20);
    randomLevelSlider.setBounds     (100, 70, getWidth() - 110, 20);
    
    readPositionLabel.setBounds     (10, 100, 90, 20);
    readPositionSlider.setBounds    (100, 100, getWidth() - 110, 20);
    
    randomPositionLabel.setBounds   (10, 130, 90, 20);
    randomPositionSlider.setBounds  (100, 130, getWidth() - 110, 20);
    
    durationLabel.setBounds         (10, 160, 90, 20);
    durationSlider.setBounds        (100, 160, getWidth() - 110, 20);
    
    randomDurationLabel.setBounds   (10, 190, 90, 20);
    randomDurationSlider.setBounds  (100, 190, getWidth() - 110, 20);
    
    pitchLabel.setBounds            (10, 220, 90, 20);
    pitchSlider.setBounds           (100, 220, getWidth() - 110, 20);
    
    randomPitchLabel.setBounds      (10, 250, 90, 20);
    randomPitchSlider.setBounds     (100, 250, getWidth() - 110, 20);
    
    densityLabel.setBounds          (10, 280, 90, 20);
    densitySlider.setBounds         (100, 280, getWidth() - 110, 20);
    
    randomPanLabel.setBounds        (10, 310, 90, 20);
    randomPanSlider.setBounds       (100, 310, getWidth() - 110, 20);
    
    freezeButton.setBounds          (10, 340, getWidth() - 20, 20);
}

void GranularPluginAudioProcessorEditor::sliderValueChanged (Slider* slider)
{
    if (slider == &inputLevelSlider)
    {
        const float inputLevel = inputLevelSlider.getValue();
        processor.setInputLevel (inputLevel);
    }
    
    if (slider == &blendSlider)
    {
        const float blend = blendSlider.getValue();
        processor.setBlend (blend);
    }
    
    if (slider == &randomLevelSlider)
    {
        const float randomLevel = randomLevelSlider.getValue();
        processor.setRandomLevel (randomLevel);
    }
    
    if (slider == &readPositionSlider)
    {
        const float readPosition = readPositionSlider.getValue();
        processor.setReadPosition (readPosition);
    }
    
    if (slider == &randomPositionSlider)
    {
        const float randomPosition = randomPositionSlider.getValue();
        processor.setRandomPosition (randomPosition);
    }
    
    if (slider == &durationSlider)
    {
        const float duration = durationSlider.getValue();
        processor.setDuration (duration);
    }
    
    if (slider == &randomDurationSlider)
    {
        const float randomDuration = randomDurationSlider.getValue();
        processor.setRandomDuration (randomDuration);
    }
    
    if (slider == &pitchSlider)
    {
        const float pitch = pitchSlider.getValue();
        processor.setPitch (pitch);
    }
    
    if (slider == &randomPitchSlider)
    {
        const float randomPitch = randomPitchSlider.getValue();
        processor.setRandomPitch (randomPitch);
    }
    
    if (slider == &densitySlider)
    {
        const float density = densitySlider.getValue();
        processor.setDensity (density);
    }
    
    if (slider == &randomPanSlider)
    {
        const float randomPan = randomPanSlider.getValue();
        processor.setRandomPan (randomPan);
    }
}

void GranularPluginAudioProcessorEditor::buttonClicked (Button *button)
{
    if (button == &freezeButton)
    {
        if (*processor.freezeParam == false)
        {
            processor.setFreeze (true);
            freezeButton.setButtonText ("Unfreeze");
        }
        else
        {
            processor.setFreeze (false);
            freezeButton.setButtonText ("Freeze");
        }
    }
}

void GranularPluginAudioProcessorEditor::updateFreezeButtonText()
{
    if (*processor.freezeParam == true)
    {
        freezeButton.setButtonText("Unfreeze");
    }
    else
    {
        freezeButton.setButtonText("Freeze");
    }
}

void GranularPluginAudioProcessorEditor::timerCallback ()
{
    inputLevelSlider.setValue       (*processor.inputLevelParam, dontSendNotification);
    blendSlider.setValue            (*processor.blendParam, dontSendNotification);
    randomLevelSlider.setValue      (*processor.randomLevelParam, dontSendNotification);
    readPositionSlider.setValue     (*processor.readPositionParam, dontSendNotification);
    randomPositionSlider.setValue   (*processor.randomPositionParam, dontSendNotification);
    durationSlider.setValue         (*processor.durationParam, dontSendNotification);
    randomDurationSlider.setValue   (*processor.randomDurationParam, dontSendNotification);
    pitchSlider.setValue            (*processor.pitchParam, dontSendNotification);
    randomPitchSlider.setValue      (*processor.randomPitchParam, dontSendNotification);
    densitySlider.setValue          (*processor.densityParam, dontSendNotification);
    updateFreezeButtonText();
}
