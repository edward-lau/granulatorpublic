# Granulator

A granular delay plugin programmed in C++ using the JUCE framework.
The bulk of the DSP code can be found in Granulator.cpp and Grain.cpp.

More information here:
http://edward-lau.com/granulator.html
